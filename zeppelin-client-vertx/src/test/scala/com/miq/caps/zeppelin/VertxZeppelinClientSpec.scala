package com.miq.caps.zeppelin

import com.miq.caps.zeppelin.domain.{CreateNotebookParam, NotebookInfo}
import io.vertx.lang.scala.VertxExecutionContext
import io.vertx.scala.core.{Vertx, VertxOptions}
import org.scalatest.FlatSpec

import scala.concurrent.duration._

class VertxZeppelinClientSpec extends FlatSpec {

  trait VertxContext {
    implicit val vertx: Vertx = {
      val options: VertxOptions = VertxOptions()
        .setWorkerPoolSize(10)
      Vertx.vertx(options)
    }

    implicit val vertexExecutionContext: VertxExecutionContext = {
      val context = vertx.getOrCreateContext()
      VertxExecutionContext(context)
    }

    implicit val timeout: Duration = 1 minute
  }

  "VertxZeppelinClient" should "fetch list of notebooks synchronously" in new VertxContext {
    val client = new VertxZeppelinClient()
    val notebooks = client.getNotebooks()
    assert(notebooks.isInstanceOf[Seq[NotebookInfo]])
  }

  it should "create a new notebook" in new VertxContext {
    val client = new VertxZeppelinClient()
    val param = CreateNotebookParam("test-notebook")
    val id = client.createNotebook(param)
    val notebook = client.getNotebooks().filter(n => n.name == "test-notebook")
    assert(id.isDefined)
    assert(notebook.exists(n => n.id == id.get)) // FIXME: Unsafe get operation here
  }

}
