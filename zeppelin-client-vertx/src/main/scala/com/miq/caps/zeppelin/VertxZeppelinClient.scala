package com.miq.caps.zeppelin

import com.miq.caps.zeppelin.ZeppelinResponseTypes.{ListMapString, MapString}
import com.miq.caps.zeppelin.ZeppelinRestPaths._
import com.miq.caps.zeppelin.domain.NullableImplicits._
import com.miq.caps.zeppelin.domain.ParseableImplicits._
import com.miq.caps.zeppelin.domain._
import com.typesafe.config.{Config, ConfigFactory}
import com.typesafe.scalalogging.LazyLogging
import io.vertx.core.buffer.Buffer
import io.vertx.core.json.JsonObject
import io.vertx.lang.scala.VertxExecutionContext
import io.vertx.scala.core.Vertx
import io.vertx.scala.ext.web.client.{HttpResponse, WebClient, WebClientOptions}

import scala.concurrent.duration.Duration
import scala.concurrent.{Await, Future}

class VertxZeppelinClient(config: Config = ConfigFactory.load)(implicit vertx: Vertx, ec: VertxExecutionContext, timeout: Duration) extends ZeppelinClient with LazyLogging {

  private val client: WebClient = {
    val options: WebClientOptions = WebClientOptions()
      .setDefaultHost(config.getString("zeppelin.host"))
      .setDefaultPort(config.getInt("zeppelin.port"))
      .setConnectTimeout(config.getInt("vertx.client.connectTimeout"))
      .setKeepAlive(config.getBoolean("vertx.client.keepAlive"))
    WebClient.create(vertx, options)
  }

  private def getJsonObject(jsonString: String): JsonObject = {
    new JsonObject(jsonString)
  }

  private def requestFuture[A, B](param: A, client: WebClient)(f: WebClient => A => Future[HttpResponse[Buffer]])(g: String => B)(implicit ev: Nullable[B]): Future[B] = {
    f(client)(param).map(r => r.bodyAsString)
      .flatMap {
        case Some(jsonString) =>
          val result: B = g(jsonString)
          Future.successful(result)
        case None =>
          // this happens when we hit an incorrect path
          Future.successful(ev.empty)
      }.recoverWith {
      case e: Throwable =>
        logger.error(e.getMessage)
        Future.successful(ev.empty)
    }
  }

  def listNotebooksFuture(): Future[Seq[NotebookInfo]] = {
    requestFuture((), client) {
      client: WebClient =>
        u: Unit => client.get(getNotebookPath())
          .sendFuture()
    } { jsonString: String =>
      val response = ZeppelinClientUtils.getJsonResponse[ListMapString](jsonString)
      val list = ZeppelinClientUtils.getListMapStringResponse[NotebookInfo](response.getBody)
      list
    }
  }

  override def getNotebooks(): Seq[NotebookInfo] = {
    Await.result(listNotebooksFuture(), timeout)
  }

  def createNotebookFuture(param: CreateNotebookParam): Future[Option[String]] = {
    requestFuture((param), client) {
      client: WebClient =>
        param: CreateNotebookParam =>
          val jsonObject = getJsonObject(param.toJson)
          client.post(getNotebookPath())
            .sendJsonObjectFuture(jsonObject)
    } { jsonString: String =>
      val response = ZeppelinClientUtils.getJsonResponse[String](jsonString)
      val id = response.getBody
      Option(id)
    }
  }

  override def createNotebook(param: CreateNotebookParam): Option[String] = {
    Await.result(createNotebookFuture(param), timeout)
  }

  def getNotebookStatusFuture(noteId: String): Future[Seq[ParagraphInfo]] = {
    requestFuture((noteId), client) {
      client: WebClient =>
        noteId: String => client.get(getNotebookJobPath(noteId))
          .sendFuture()
    } { jsonString: String =>
      val response = ZeppelinClientUtils.getJsonResponse[ListMapString](jsonString)
      val list = ZeppelinClientUtils.getListMapStringResponse[ParagraphInfo](response.getBody)
      list
    }
  }

  override def getNotebookStatus(noteId: String): Seq[ParagraphInfo] = {
    Await.result(getNotebookStatusFuture(noteId), timeout)
  }

  def getNotebookFuture(noteId: String): Future[Option[Notebook]] = {
    requestFuture((noteId), client) {
      client: WebClient =>
        noteId: String => client.get(getNotebookPath(noteId))
          .sendFuture()
    } { jsonString: String =>
      val response = ZeppelinClientUtils.getJsonResponse[MapString](jsonString)
      val body = ZeppelinClientUtils.getMapStringResponse[Notebook](response.getBody)
      Option(body)
    }
  }

  override def getNotebook(noteId: String): Option[Notebook] = {
    Await.result(getNotebookFuture(noteId), timeout)
  }

  def deleteNotebookFuture(noteId: String): Future[Boolean] = {
    requestFuture((noteId), client) {
      client: WebClient =>
        noteId: String => client.delete(getNotebookPath(noteId))
          .sendFuture()
    } { jsonString: String =>
      true
    }
  }

  override def deleteNotebook(noteId: String): Boolean = {
    Await.result(deleteNotebookFuture(noteId), timeout)
  }

  def getParagraphFuture(noteId: String, paragraphId: String): Future[Option[Paragraph]] = {
    requestFuture((noteId, paragraphId), client) {
      client: WebClient =>
        a: (String, String) => client.get(getParagraphPath(a._1, a._2))
          .sendFuture()
    } { jsonString: String =>
      val response = ZeppelinClientUtils.getJsonResponse[MapString](jsonString)
      val body = ZeppelinClientUtils.getMapStringResponse[Paragraph](response.getBody)
      Option(body)
    }
  }

  override def getParagraph(noteId: String, paragraphId: String): Option[Paragraph] = {
    Await.result(getParagraphFuture(noteId, paragraphId), timeout)
  }

  def runNotebookFuture(noteId: String): Future[Boolean] = {
    requestFuture(noteId, client) {
      client: WebClient =>
        noteId: String => client.post(getNotebookJobPath(noteId))
          .sendFuture()
    } { jsonString: String =>
      val response = ZeppelinClientUtils.getJsonResponse[String](jsonString)
      println(response)
      true
    }
  }

  override def runNotebook(noteId: String): Boolean = {
    Await.result(runNotebookFuture(noteId), timeout)
  }

}
