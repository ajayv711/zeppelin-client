package com.miq.caps.zeppelin

import com.miq.caps.zeppelin.domain.{CreateNotebookParam, CreateParagraphParam}
import com.typesafe.config.ConfigFactory
import com.typesafe.scalalogging.LazyLogging
import io.vertx.lang.scala.VertxExecutionContext
import io.vertx.scala.core.{Vertx, VertxOptions}

import scala.concurrent.duration._

object ZeppelinTest extends App with LazyLogging {

  val config = ConfigFactory.load()

  implicit val vertx: Vertx = {
    val options: VertxOptions = VertxOptions()
      .setWorkerPoolSize(10)
    Vertx.vertx(options)
  }

  implicit val vertexExecutionContext: VertxExecutionContext = {
    val context = vertx.getOrCreateContext()
    VertxExecutionContext(context)
  }

  implicit val timeout: Duration = 1 minute

  val zeppelinClient: VertxZeppelinClient = new VertxZeppelinClient(config)

  zeppelinClient.getNotebooks().foreach(n => logger.info("{}", n))

  val param = CreateNotebookParam("sample", Array(CreateParagraphParam("hello", "println(4)")))
  zeppelinClient.createNotebook(param).foreach(id => logger.info("{}", id))

  val noteId = "2DHBXJ49Z"
  val paragraphId = "20180525-200207_1922678685"

  zeppelinClient.getNotebookStatus(noteId).foreach(n => logger.info("{}", n))
  zeppelinClient.getNotebook(noteId).foreach(n => logger.info("{}", n))
  zeppelinClient.getParagraph(noteId, paragraphId).foreach(p => logger.info("{}", p))
  logger.info("{}", zeppelinClient.deleteNotebook(noteId))

  val transformNoteId = "2DHKMDQPB"
  logger.info("{}", zeppelinClient.runNotebook(noteId))

  vertx.close()

}
