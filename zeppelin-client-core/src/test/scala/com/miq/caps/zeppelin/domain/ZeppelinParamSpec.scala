package com.miq.caps.zeppelin.domain

import org.scalatest.FlatSpec

class ZeppelinParamSpec extends FlatSpec {

  "A ZeppelinParam instance" should "be converted to json" in {
    val param1 = CreateNotebookParam("test-notebook")
    val param2 = CreateNotebookParam("test-notebook", Array(CreateParagraphParam("test-paragraph", """println("Hello, World")""")))

    val param1json = "{\"name\":\"test-notebook\",\"paragraphs\":[]}"
    val param2json = "{\"name\":\"test-notebook\",\"paragraphs\":[{\"title\":\"test-paragraph\",\"text\":\"println(\\\"Hello, World\\\")\"}]}"

    assert(param1.toJson == param1json)
    assert(param2.toJson == param2json)
  }

}
