package com.miq.caps.zeppelin.domain

trait Nullable[T] {
  def empty: T
}

object NullableImplicits {

  implicit def nullableOption[T]: Nullable[Option[T]] = new Nullable[Option[T]] {
    override def empty: Option[T] = None
  }

  implicit def nullableSeq[T]: Nullable[Seq[T]] = new Nullable[Seq[T]] {
    override def empty: Seq[T] = Seq.empty[T]
  }

  implicit val nullableBoolean: Nullable[Boolean] = new Nullable[Boolean] {
    override def empty: Boolean = false
  }

}