package com.miq.caps.zeppelin.domain

import java.text.SimpleDateFormat

import com.miq.caps.zeppelin.ZeppelinClientUtils
import com.miq.caps.zeppelin.ZeppelinResponseTypes.{ListMapString, MapString}

trait Parseable[T] {
  def parse(m: Map[String, Any]): T
}

object ParseableImplicits {

  private val dateTimeParser = new SimpleDateFormat("EEE MMM dd HH:mm:ss zzzz yyyy")
  private val paragraphTimeParser = new SimpleDateFormat("MMM dd, yyyy h:mm:ss a")

  implicit def anyToString(a: Any): String = a.toString

  implicit val notebookInfoParseable: Parseable[NotebookInfo] {
    def parse(m: Map[String, Any]): NotebookInfo
  } = new Parseable[NotebookInfo] {
    override def parse(m: Map[String, Any]): NotebookInfo = {
      val name = m("name")
      val id = m("id")
      NotebookInfo(name, id)
    }
  }

  def getStatus(status: String): Status = status match {
    case "FINISHED" => Finished
    case "RUNNING" => Running
    case "READY" => Ready
    case "FAILED" => Failed
    case "SUCCESS" => Success
  }

  def getMessageType(messageType: String): MessageType = messageType match {
    case "TABLE" => Table
    case "TEXT" => Text
  }

  implicit val paragraphInfoParseable: Parseable[ParagraphInfo] {
    def parse(m: Map[String, Any]): ParagraphInfo
  } = new Parseable[ParagraphInfo] {
    override def parse(m: Map[String, Any]): ParagraphInfo = {
      val id = m("id")
      val status = getStatus(m("status"))
      val finished = m.get("finished").map(dateTimeParser.parse(_))
      val started = m.get("started").map(dateTimeParser.parse(_))
      ParagraphInfo(id, status, finished, started)
    }
  }

  implicit val messageParseable: Parseable[Message] {
    def parse(m: Map[String, Any]): Message
  } = new Parseable[Message] {
    override def parse(m: Map[String, Any]): Message = {
      val messageType = getMessageType(m("type"))
      val data = m("data")
      Message(messageType, data)
    }
  }

  implicit val resultParseable: Parseable[Result] {
    def parse(m: Map[String, Any]): Result
  } = new Parseable[Result] {
    override def parse(m: Map[String, Any]): Result = {
      val code = getStatus(m("code"))
      val msg = m("msg") match {
        case m: ListMapString => ZeppelinClientUtils.getListMapStringResponse[Message](m)
        case _ => Seq.empty
      }
      Result(code, msg)
    }
  }

  implicit val paragraphParseable: Parseable[Paragraph] {
    def parse(m: Map[String, Any]): Paragraph
  } = new Parseable[Paragraph] {
    override def parse(m: Map[String, Any]): Paragraph = {
      val id = m("id")
      val jobName = m("jobName")
      val text = m.get("text").map(_.toString)
      val result = m.get("results") match {
        case Some(m: MapString) => Some(ZeppelinClientUtils.getMapStringResponse[Result](m))
        case _ => None
      }
      val dateCreated = m.get("dateCreated").map(paragraphTimeParser.parse(_))
      val dateStarted = m.get("dateStarted").map(paragraphTimeParser.parse(_))
      val dateFinished = m.get("dateFinished").map(paragraphTimeParser.parse(_))
      val status = getStatus(m("status"))
      Paragraph(id, jobName, text, result, dateCreated, dateStarted, dateFinished, status)
    }
  }
  implicit val notebookParseable: Parseable[Notebook] {
    def parse(m: Map[String, Any]): Notebook
  } = new Parseable[Notebook] {
    override def parse(m: Map[String, Any]): Notebook = {
      val id = m("id")
      val name = m("name")
      val paragraphs = m("paragraphs") match {
        case m: ListMapString => ZeppelinClientUtils.getListMapStringResponse[Paragraph](m)
        case _ => Seq.empty
      }
      Notebook(id, name, paragraphs)
    }
  }
}