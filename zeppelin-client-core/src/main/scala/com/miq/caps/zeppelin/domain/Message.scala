package com.miq.caps.zeppelin.domain

case class Message(messageType: MessageType, data: String)