package com.miq.caps.zeppelin.domain

case class Result(code: Status, msg: Seq[Message])