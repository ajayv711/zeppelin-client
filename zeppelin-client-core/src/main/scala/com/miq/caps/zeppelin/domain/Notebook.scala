package com.miq.caps.zeppelin.domain

case class Notebook(id: String, name: String, paragraphs: Seq[Paragraph])