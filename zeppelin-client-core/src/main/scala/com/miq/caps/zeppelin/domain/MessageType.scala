package com.miq.caps.zeppelin.domain

sealed trait MessageType
case object Table extends MessageType
case object Text extends MessageType