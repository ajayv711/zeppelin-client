package com.miq.caps.zeppelin.domain

case class NotebookInfo(name: String, id: String)