package com.miq.caps.zeppelin

import com.google.gson.Gson
import com.miq.caps.zeppelin.ZeppelinResponseTypes.{ListMapString, MapString}
import com.miq.caps.zeppelin.domain._
import org.apache.zeppelin.server.JsonResponse

import scala.collection.JavaConverters._

trait ZeppelinClient {

  def getNotebooks(): Seq[NotebookInfo]

  def createNotebook(param: CreateNotebookParam): Option[String]

  def getNotebookStatus(noteId: String): Seq[ParagraphInfo]

  def getNotebook(noteId: String): Option[Notebook]

  def deleteNotebook(noteId: String): Boolean

  def getParagraph(noteId: String, paragraphId: String): Option[Paragraph]

  def runNotebook(noteId: String): Boolean

}

object ZeppelinClientUtils {

  private val gson: Gson = new Gson()

  private [zeppelin] def getJsonResponse[T](response: String): JsonResponse[T] = {
    gson.fromJson(response, classOf[JsonResponse[T]])
  }

  private [zeppelin] def getListMapStringResponse[T](listMapString: ListMapString)(implicit ev: Parseable[T]): Seq[T] = {
    val body = listMapString.asScala
    val list = body.map(m => ev.parse(m.asScala.toMap))
    list
  }

  private [zeppelin] def getMapStringResponse[T](mapString: MapString)(implicit ev: Parseable[T]): T = {
    val body = mapString.asScala
    val result = ev.parse(body.toMap)
    result
  }

}