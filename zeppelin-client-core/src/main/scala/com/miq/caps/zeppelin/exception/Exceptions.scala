package com.miq.caps.zeppelin.exception

object Exceptions {

  class ZeppelinServerException extends Exception("Zeppelin Server is unreachable")

  class ZeppelinNotebookException extends Exception("Zeppelin Notebook could not be created")

}
