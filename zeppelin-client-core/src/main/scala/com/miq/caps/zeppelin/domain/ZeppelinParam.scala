package com.miq.caps.zeppelin.domain

import com.miq.caps.zeppelin.JsonUtils

trait ZeppelinParam[T] {

  def toJson(): String = JsonUtils.toJson(this)

}

case class CreateParagraphParam(title: String, text: String) extends ZeppelinParam[CreateParagraphParam]

case class CreateNotebookParam(name: String, paragraphs: Array[CreateParagraphParam] = Array.empty[CreateParagraphParam]) extends ZeppelinParam[CreateNotebookParam]