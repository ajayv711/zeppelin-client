package com.miq.caps.zeppelin.domain

import java.util.Date

case class Paragraph(id: String, jobName: String, text: Option[String], results: Option[Result], dateCreated: Option[Date], dateStarted: Option[Date], dateFinished: Option[Date], status: Status)