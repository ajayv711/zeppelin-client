package com.miq.caps.zeppelin

object ZeppelinResponseTypes {

  type ListMapString = java.util.List[java.util.Map[String, Any]]
  type MapString = java.util.Map[String, Any]

}
