package com.miq.caps.zeppelin.domain

sealed trait Status
case object Finished extends Status
case object Running extends Status
case object Ready extends Status
case object Failed extends Status
case object Success extends Status