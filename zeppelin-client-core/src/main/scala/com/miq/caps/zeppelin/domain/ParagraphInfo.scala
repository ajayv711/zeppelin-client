package com.miq.caps.zeppelin.domain

import java.util.Date

case class ParagraphInfo(id: String, status: Status, finished: Option[Date], started: Option[Date])
