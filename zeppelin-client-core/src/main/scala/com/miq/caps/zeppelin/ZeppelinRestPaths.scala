package com.miq.caps.zeppelin

object ZeppelinRestPaths {

  private val base: String = "/api"

  private val NOTEBOOK: String = base + "/notebook"

  def getNotebookPath(): String = NOTEBOOK

  def getNotebookPath(noteId: String): String = NOTEBOOK + "/" + noteId

  def getParagraphPath(noteId: String, paragraphId: String): String = getNotebookPath(noteId) + "/paragraph/" + paragraphId

  def getNotebookJobPath(noteId: String): String = NOTEBOOK + "/job/" + noteId

}
