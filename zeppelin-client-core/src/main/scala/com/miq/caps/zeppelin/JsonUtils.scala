package com.miq.caps.zeppelin

import com.google.gson._

object JsonUtils {

  private val gson: Gson = new GsonBuilder()
    .create()

  def toJson[T](t: T): String = {
    gson.toJson(t)
  }

  // FIXME: class type require but T found error during compilation
  // def fromJson[T](json: String)(implicit ct: ClassTag[T]): T = gson.fromJson(json, ct.runtimeClass)

}
