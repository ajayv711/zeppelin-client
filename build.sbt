name := "zeppelin-client"
organization in ThisBuild := "com.miq.caps"
version in ThisBuild := "0.1.0"
scalaVersion in ThisBuild := "2.12.4"

lazy val global = (project in file("."))
  .settings(settings)
  .aggregate(core, vertx)

lazy val core = (project in file("zeppelin-client-core"))
  .settings(
    name := "zeppelin-client-core",
    settings,
    libraryDependencies ++= commonDependencies ++ coreDependencies
  )

lazy val vertx = (project in file("zeppelin-client-vertx"))
  .settings(
    name := "zeppelin-client-vertx",
    settings,
    libraryDependencies ++= commonDependencies ++ vertxDependencies
  )
  .dependsOn(core)

lazy val test = (project in file("zeppelin-client-test"))
  .settings(
    name := "zeppelin-client-test",
    settings,
    libraryDependencies ++= commonDependencies
  )
  .dependsOn(core, vertx)

lazy val dependencies = new {
  val zeppelinVersion = "0.7.3"
  val vertxVersion = "3.5.0"
  val typesafeConfigVersion = "1.3.2"
  val scalaLoggingVersion = "3.8.0"
  val logbackVersion = "1.2.3"
  val scalatestVersion = "3.0.0"
  val scalacheckVersion = "1.14.0"

  val zeppelinServer = "org.apache.zeppelin" % "zeppelin-server" % zeppelinVersion
  val vertxWebClientScala = "io.vertx" %% "vertx-web-client-scala" % vertxVersion
  val typesafeConfig = "com.typesafe" % "config" % typesafeConfigVersion
  val scalaLogging = "com.typesafe.scala-logging" %% "scala-logging" % scalaLoggingVersion
  val logback = "ch.qos.logback" % "logback-classic" % logbackVersion
  val scalatest = "org.scalatest" %% "scalatest" % scalatestVersion
  val scalacheck = "org.scalacheck" %% "scalacheck" % scalacheckVersion
}

lazy val commonDependencies = Seq(
  dependencies.typesafeConfig,
  dependencies.scalaLogging,
  dependencies.logback,
  dependencies.scalatest,
  dependencies.scalacheck
)

lazy val coreDependencies = Seq(
  dependencies.zeppelinServer
)

lazy val vertxDependencies = Seq(
  dependencies.vertxWebClientScala
)

lazy val settings = commonSettings

lazy val compilerOptions = Seq(
  "-unchecked",
  "-feature",
  "-language:existentials",
  "-language:higherKinds",
  "-language:implicitConversions",
  "-language:postfixOps",
  "-deprecation",
  "-encoding",
  "utf8"
)
lazy val commonSettings = Seq(
  scalacOptions ++= compilerOptions,
  resolvers ++= Seq(
    "Local Maven Repository" at "file://" + Path.userHome.absolutePath + "/.m2/repository",
    Resolver.sonatypeRepo("releases"),
    Resolver.sonatypeRepo("snapshots")
  )
)