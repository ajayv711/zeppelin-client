# Zeppelin Client
An SDK for the Zeppelin REST API

## Motivation
+ Abstracts away the handling of requests and responses to the Zeppelin Server REST API
+ No more dealing with ad-hoc requests
+ Need for a fluent API to use Zeppelin for Notebook Service
+ The choice of Web Client is left to the user for greater flexibility and promote code reusage
+ Implement clients for `vertx`, `akka`, `okhttp` and more

## Usage
Currently `vertx` client has been implemented as `zeppelin-client-vertx`

**Until the project gets published to a public repository, you have to publish the dependencies locally**

#### Prerequisites
+ JDK 1.8
+ SBT version `1.0.0` or above
+ Scala SDK `2.12.4` in your IDE
+ Apache Zeppelin `0.7.3` installed on your local system

#### Preparing dependencies

```bash
git clone git@bitbucket.org:ajayv711/zeppelin-client.git
cd zeppelin-client

# If creating JARs and adding to lib
sbt clean compile package

# If you'd like to publish JARs to local maven repository
sbt clean publishLocal
```

#### Using zeppelin-client in your project
For now, only `scala` projects are supported

+ Create a new `scala` project from your favourite IDE with JDK 8 and SBT version `1.0.0` and Scala version `2.12.4`
+ Add the following to your `build.sbt` file

```
scalaVersion := "2.12.4"

libraryDependencies ++= Seq(
  "com.miq.caps" %% "zeppelin-client-vertx" % "0.1.0"
)
```
+ Create an executable Application in `src/main/scala/com/miq/caps/ZeppelinClientDemo.scala`

```scala
package com.miq.caps

import com.miq.caps.zeppelin.VertxZeppelinClient
import io.vertx.lang.scala.VertxExecutionContext
import io.vertx.scala.core.{Vertx, VertxOptions}

import scala.concurrent.duration._

object ZeppelinClientDemo extends App {

  // Initialize Vertx
  implicit val vertx: Vertx = {
    val options: VertxOptions = VertxOptions()
      .setWorkerPoolSize(10)
    Vertx.vertx(options)
  }

  // Execution Context is needed for async requests
  implicit val vertexExecutionContext: VertxExecutionContext = {
    val context = vertx.getOrCreateContext()
    VertxExecutionContext(context)
  }

  // Set a default timeout for fetching requests from Zeppelin Server
  implicit val timeout: Duration = 1 minute

  val zeppelinClient: VertxZeppelinClient = new VertxZeppelinClient()

  zeppelinClient.listNotebooks().foreach(println)

  // Clean up after
  vertx.close()

}
```

+ Before running make sure you have Apache Zeppelin running
+ You can set configurations in `src/main/resources/application.conf`

```
// default configurations that you can override
zeppelin {
  host = "localhost"
  port = 8090
}

vertx {
  client {
    connectTimeout = 60
    keepAlive = false
  }
}
```

+ Run your demo application by calling `sbt run`
